import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'home',
                title    : 'Home',
                translate: 'NAV.HOME.BTN_NAME',
                type     : 'item',
                icon     : 'home',
                url      : '/home',
            },
            {
                id       : 'servicios',
                title    : 'Servicios',
                translate: 'NAV.SERVICE.BTN_NAME',
                type     : 'collapsable',
                icon     : 'dashboard',
                children : [
                    {
                        id   : 'list_services',
                        title: 'NAV.SERVICE.LISTSERVICES',
                        type : 'item',
                        url  : '/service/listService'
                    },
                    {
                        id   : 'add_service',
                        title: 'NAV.SERVICE.ADDSERVICE',
                        type : 'item',
                        url  : '/service/addService'
                    },
                    {
                        id   : 'list_category',
                        title: 'NAV.SERVICE.LISTCATEGORY',
                        type : 'item',
                        url  : '/service/listCategory'
                    },
                    {
                        id   : 'add_category',
                        title: 'NAV.SERVICE.ADDCATEGORY',
                        type : 'item',
                        url  : '/service/addCategory'
                    }
                ]
            }
        ]
    }
];
