export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            },
            'HOME': {
                'BTN_NAME' : 'Inicio'
            },
            'SERVICE': {
                'BTN_NAME' : 'Servicios',
                'ADDCATEGORY' : 'Agregar Categoria',
                'LISTCATEGORY' : 'Ver Categorias',
                'ADDSERVICE' : 'Agregar Servicios',
                'LISTSERVICES' : 'Ver Servicios'
            }
        }
    }
};
