import { Component, OnInit, Injectable } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { CategoryService } from '../category.service';
import { Observable } from 'rxjs';
import { locale as english } from '../i18n/en';
import { locale as turkish } from '../i18n/tr';
import {  AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Category } from "../Category";
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { map, filter, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DeleteComponent } from '../delete/delete.component';
import { ifError } from 'assert';

@Component({
  selector: 'list-service',
  templateUrl: './list-service.component.html',
  styleUrls: ['./list-service.component.scss']
})
export class ListServiceComponent
{
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  items: Observable<any[]>;
   ids: any[];
   
   list: Category[];
   todoDetails : AngularFireObject<Category>;

  itemRef: AngularFireObject<any>;
  itemAux: Observable<any>;
  
  todo$: Observable<Category[]>;
  clean: any[]; 
  constructor(
    public db: AngularFireDatabase,
    private categoryService: CategoryService,
    private router: Router,
    public dialog: MatDialog,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  )
  {  
    this.itemRef = db.object('services');
    this.itemRef.snapshotChanges().subscribe(action => {    
      var aux: any[] = action.payload.val();
      this.clean = [];
      for(var index in aux) {        
        this.clean.push({
          id: index,
          name: aux[index].name,
          categories: aux[index].categories,
          description: aux[index].description,
          picture: aux[index].picture,
        });
      }
      });
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
 }

 edit(id: string){
  this.router.navigate(['service/editService/' + id]);
 }

 delete(id: string, collection: string){
  let delRef: AngularFireObject<any> = this.db.object(collection + '/' + id);
  delRef.remove();
  if(collection == 'superCategory'){
    this.router.navigate(['service/listCategory']);
  }
  if(collection == 'service'){
    this.router.navigate(['service/listService']);
  }
 }

 deleteConfirm (inId: string, inCollection: string): void {
  const dialogRef = this.dialog.open(DeleteComponent, {      
    data: {id: inId, collection: inCollection}
  });

  dialogRef.afterClosed().subscribe(result => {
    if(result.id){
      this.delete(result.id, result.collection);
    }
  });
}
}