import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage} from '@angular/fire/storage';
import { Category } from '../Category';
import { CategoryService } from '../category.service';

import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../i18n/en';
import { locale as turkish } from '../i18n/tr';



@Component({
  selector: 'add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent
{
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  
  picture:string;
  category: Category;
  submited = false;
  categoryName = new FormControl('', [Validators.required]);
  categoryDescription = new FormControl('', [Validators.required]);
  categoryPicture = new FormControl('', []);
  id = '';
  file = File; 
  filePath = '';
  items: Observable<any[]>;
  @ViewChild('imagesService', null) inputImageService: ElementRef;
  private uploadPercent: Observable<number>;
  public urlImage: Observable<string>;

  constructor(
    public db: AngularFireDatabase,
    private categoryService: CategoryService,
    private afstorage: AngularFireStorage,
    private router: Router,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  )
  {
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }

  onUpload(e){
      this.id  = Math.random().toString(36).substring(2);
      this.file = e.target.files[0];
      this.filePath = `/serviceImages/serviceImage_${this.id}`;
      const ref = this.afstorage.ref(this.filePath);
      const task = this.afstorage.upload(this.filePath, this.file);
      this.uploadPercent = task.percentageChanges();
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload=(event:any)=>{
        this.picture = event.target.result;
      }


      task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();
    }

  onSubmit(){
    this.category = new Category();
    this.category.name = this.categoryName.value;
    this.category.description = this.categoryDescription.value;
    this.category.picture = this.inputImageService.nativeElement.value;
    this.categoryService.createCategory(this.category);    
    this.router.navigate(['service/listCategory']);
  }
}