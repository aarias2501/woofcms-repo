import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material'
import { Inject } from '@angular/core';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  id: string;
  collection: string;
  service: boolean = false;
  category: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DeleteComponent>) {
    this.id = data.id;
    this.collection = data.collection;
    if(this.collection == 'service'){
      this.service = true;
      this.category = false;
    }
    if(this.collection == 'superCategory'){
      this.service = false;
      this.category = true;
    }
   }

  ngOnInit() {
  }

  callDelete() {
    this.dialogRef.close({id: this.id, collection: this.collection});
  }

  close() {
    this.dialogRef.close({});
  }

}
