import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';


import { Category } from "../Category";
import { AppService } from "../appService";
import { CategoryService } from '../category.service';

import { locale as english } from '../i18n/en';
import { locale as turkish } from '../i18n/tr';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

import { map, filter, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DeleteComponent } from '../delete/delete.component';
import { ifError } from 'assert';
import { FormControl, Validators } from '@angular/forms';


import { ActivatedRoute } from "@angular/router";



import { AngularFireStorage} from '@angular/fire/storage';




@Component({
  selector: 'app-edit-service',
  templateUrl: './edit-service.component.html',
  styleUrls: ['./edit-service.component.scss']
})

export class EditServiceComponent {
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  
  //  ref: AngularFirestoreCollection<Category>;
   
  items: Observable<any[]>;
   ids: any[];
   id2:string;
   id:string;
  checkList= new Array();
  types = new AppService();
  types2 = new AppService();
  todoDetails : AngularFireObject<Category>;
  category = new Category();
  itemRef: AngularFireObject<any>;
  itemAux: Observable<any>;
  name:string;
  
  description:string;
  picture:string;
  todo$: Observable<Category[]>;
  clean: any[]; 
  categoryName = new FormControl('', [Validators.required]);
  categoryDescription = new FormControl('', [Validators.required]);
  categoryPicture = new FormControl('', []);
  @ViewChild('imagesService', null) inputImageService: ElementRef;
  private uploadPercent: Observable<number>;
  public urlImage: Observable<string>;
  filePath = '';


  private basePath = '/serviceImages';
  file: File;
  url = '';


  constructor(

    public db: AngularFireDatabase,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private afstorage: AngularFireStorage,
    public dialog: MatDialog,
    
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  )
  {  
    this.items = this.db.list('superCategory').valueChanges();

    
  

    this.id2 = this.route.snapshot.paramMap.get("id");
    this.itemRef = db.object('services/');
    this.itemRef.snapshotChanges().subscribe(action => {    
      var aux: any[] = action.payload.val();
      this.clean = [];
      for(var index in aux) {        

        if(index == this.id2){
          this.name = aux[index].name;
          this.description = aux[index].description;
          this.types.categories = aux[index].categories,
          this.picture = aux[index].picture;

          this.types.name = aux[index].name;
          this.types.description = aux[index].description;
          this.types.categories = aux[index].categories,
          this.types.picture = aux[index].picture;

        }
        this.clean.push({
          id: index,
          data:[{
          name: aux[index].name,
          description: aux[index].description,
          categories: aux[index].categories,
          picture: aux[index].picture
          }]
          
        });
      }
      });

      this.itemRef = db.object('superCategory/');
      this.itemRef.snapshotChanges().subscribe(action => {    
        var aux: any[] = action.payload.val();
        this.clean = [];
        for(var index in aux) {
          this.checkList.push({
            name: aux[index].name,
            picture: aux[index].picture,
            check:false
          });
        }

        for(let auxII in this.types.categories){
          for(let auxI in this.checkList){
            if(this.types.categories[auxII] ==  this.checkList[auxI].name){
              this.checkList[auxI].check = true;
            }
          }
        }

        });


       




      
  
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
 }


 handleFiles(event) {
  this.file = event.target.files[0];
}

isOnList(name){
  return true;

}

async uploadFile() {
  if (this.file) {
    const filePath = `${this.basePath}/${this.file.name}`;    
    const snap = await this.afstorage.upload(filePath, this.file); 
    this.getUrl(snap);
  } else {alert('Please select an image'); }
}

private async getUrl(snap: firebase.storage.UploadTaskSnapshot) {
  const url = await snap.ref.getDownloadURL();
  this.url = url;  //store the URL
  console.log(this.url);
} 


 onUpload(e){
  this.id  = Math.random().toString(36).substring(2);
  this.file = e.target.files[0];
  console.log(e.target.files[0].name);
  this.filePath = `/serviceImages/${this.file.name}`;
  const ref = this.afstorage.ref(this.filePath);
  const task = this.afstorage.upload(this.filePath, this.file);
  
  
  this.uploadPercent = task.percentageChanges();

  var reader = new FileReader();
  reader.readAsDataURL(e.target.files[0]);
  reader.onload=(event:any)=>{
    this.picture = event.target.result;
  }




  
 


  task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();  

  
}

onBack(){
  this.router.navigate(['service/listCategory']);
}

onCheckChange(e){
  let valueName  = e.target.attributes.name.value;
  if(e.target.checked){
    if(this.types.categories.indexOf(valueName) == -1){
      this.types.categories.push(valueName)
    }  

  }else{
    if(this.types.categories.indexOf(e.target.attributes.name.value) != -1){
      
      var index = this.types.categories.indexOf(e.target.attributes.name.value);
      this.types.categories[index] = null;

      
    }  


  }
  console.log('change', this.types.categories);
  
  


  


  

}



 
onSubmit(){    
  if(this.categoryName.value == ""){
    this.types.name = this.name;
    }else{
      this.types.name = this.categoryName.value;
    }
  
    if(this.categoryDescription.value == ""){
      this.types.description = this.description;
    }else{
      this.types.description = this.categoryDescription.value;
    }
  
    if(this.inputImageService.nativeElement.value == ""){
      this.types.picture = this.picture;
    }else{
      this.types.picture = this.inputImageService.nativeElement.value;
    }
    
    let auxList = new Array();

    for(let i in this.types.categories){
      if(this.types.categories[i] != null){
        auxList.push(this.types.categories[i]);
      }
    }
    this.types.categories = auxList;

    this.db.list('services').update(this.id2, this.types);  
    this.router.navigate(['service/listService']);  
 

}

 

}