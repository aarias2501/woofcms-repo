import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';


import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { HttpClient }  from '@angular/common/http';

import { Category } from "../Category";
import { CategoryService } from '../category.service';

import { locale as english } from '../i18n/en';
import { locale as turkish } from '../i18n/tr';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

import { map, filter, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DeleteComponent } from '../delete/delete.component';
import { ifError } from 'assert';
import { FormControl, Validators } from '@angular/forms';


import { ActivatedRoute } from "@angular/router";



import { AngularFireStorage} from '@angular/fire/storage';




@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})

export class EditCategoryComponent {
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  
  //  ref: AngularFirestoreCollection<Category>;
   
   items: Observable<any[]>;
   ids: any[];
   id2:string;
   id:string;
   
   todoDetails : AngularFireObject<Category>;
  category = new Category();
  itemRef: AngularFireObject<any>;
  itemAux: Observable<any>;
  name:string;
  
  description:string;
  picture:string;
  todo$: Observable<Category[]>;
  clean: any[]; 
  categoryName = new FormControl('', [Validators.required]);
  categoryDescription = new FormControl('', [Validators.required]);
  categoryPicture = new FormControl('', []);
  @ViewChild('imagesService', null) inputImageService: ElementRef;
  private uploadPercent: Observable<number>;
  public urlImage: Observable<string>;
  filePath = '';


  private basePath = '/serviceImages';
  file: File;
  url = '';


  constructor(

    public db: AngularFireDatabase,
    private categoryService: CategoryService,
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute,
    private afstorage: AngularFireStorage,
    public dialog: MatDialog,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  )
  {  
    this.id2 = this.route.snapshot.paramMap.get("id");
    this.itemRef = db.object('superCategory/');
    this.itemRef.snapshotChanges().subscribe(action => {    
      var aux: any[] = action.payload.val();
      this.clean = [];
      for(var index in aux) {        

        if(index == this.id2){
          this.name = aux[index].name;
          this.description = aux[index].description;
          this.picture = aux[index].picture;

          this.category.name = this.name;
          this.category.description = this.description;
          this.category.picture = this.picture;

     

        }
        this.clean.push({
          id: index,
          data:[{
          name: aux[index].name,
          description: aux[index].description,
          picture: aux[index].picture
          }]
          
        });
      }
      });
      
  
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
 }



 onUpload(e){
  
  this.id  = Math.random().toString(36).substring(2);
  this.file = e.target.files[0];
  console.log(e.target.files[0].name);
  this.filePath = `/serviceImages/${this.file.name}`;
  const ref = this.afstorage.ref(this.filePath);
  const task = this.afstorage.upload(this.filePath, this.file);
  this.uploadPercent = task.percentageChanges();

  var reader = new FileReader();
  reader.readAsDataURL(e.target.files[0]);
  reader.onload=(event:any)=>{
    this.picture = event.target.result;
  }



  task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();  

  
  
}

onBack(){
  this.router.navigate(['service/listCategory']);
}

 onSubmit(){    
  if(this.categoryName.value == ""){
  this.category.name = this.name;
  }else{
    this.category.name = this.categoryName.value;
  }

  if(this.categoryDescription.value == ""){
    this.category.description = this.description;
  }else{
    this.category.description = this.categoryDescription.value;
  }

  if(this.inputImageService.nativeElement.value == ""){
    this.category.picture = this.picture;
  }else{
    this.category.picture = this.inputImageService.nativeElement.value;
  }
  console.log(this.picture);


  this.db.list('superCategory').update(this.id2, this.category);  
  this.router.navigate(['service/listCategory']);  
}
}