import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

import { Category } from "./category";
import { Observable, observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private dbPath = '/superCategory';
  categoryRef: AngularFireList<Category> = null;

  constructor(private db: AngularFireDatabase) {
    this.categoryRef = db.list(this.dbPath);
  }

  createCategory(category: Category): void {
    this.categoryRef.push(category);
  }

  updateCategory(key: string, value: any): Promise<void> {
    return this.categoryRef.update(key, value);
  }

  deleteCustomer(key: string): Promise<void> {
    return this.categoryRef.remove(key);
  }

  getCategoryList(): AngularFireList<Category> {
    return this.categoryRef;
  }

  deleteAll(): Promise<void> {
    return this.categoryRef.remove();
  }

}
