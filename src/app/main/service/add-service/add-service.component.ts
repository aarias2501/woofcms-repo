import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage} from '@angular/fire/storage';
import { AppService } from "../appService";
import { AppDataService } from '../appService.service';

import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../i18n/en';
import { locale as turkish } from '../i18n/tr';

@Component({
  selector: 'add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss']
})
export class AddServiceComponent
{
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */


  service: AppService = new AppService();
  submited = false;
  categoryName = new FormControl('', [Validators.required]);
  categoryDescription = new FormControl('', [Validators.required]);
  categoryPicture = new FormControl('', []);
  picture:string;
  categoryS = new FormControl('');
  categoryB = new FormControl('');
  categoryV = new FormControl('');
  categoryH = new FormControl('');
  categoryE = new FormControl('');  
  id = '';
  file = File; 
  filePath = '';
  items: Observable<any[]>;
  @ViewChild('imagesService', null) inputImageService: ElementRef;
  private uploadPercent: Observable<number>;
  public urlImage: Observable<string>;
  private auxCategoryList: Array<string>;
  constructor(
    
    public db: AngularFireDatabase,
    private appDataService: AppDataService,
    private afstorage: AngularFireStorage,
    private router: Router,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  )
  {
    this.items = this.db.list('superCategory').valueChanges();
    this.auxCategoryList = [];
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
  }

  onUpload(e){
    this.id  = Math.random().toString(36).substring(2);
    this.file = e.target.files[0];
    this.filePath = `/serviceImages/serviceImage_${this.id}`;
    const ref = this.afstorage.ref(this.filePath);
    const task = this.afstorage.upload(this.filePath, this.file);
    this.uploadPercent = task.percentageChanges();
    var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload=(event:any)=>{
        this.picture = event.target.result;
      }

    task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();
  }

  onSubmit(){  
    this.service.name = this.categoryName.value;
    this.service.description = this.categoryDescription.value;
    this.service.picture = this.inputImageService.nativeElement.value;
    this.service.categories = this.auxCategoryList;
    this.appDataService.createCategory(this.service);     
    this.router.navigate(['service/listService']);
  }

eventCheck(event){
  console.log(event.target.checked, event.target.name);
  if(!this.auxCategoryList.includes(event.target.name)){
    this.auxCategoryList.push(event.target.name);
  }
}

}
