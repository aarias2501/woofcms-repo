import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

import { AppService } from "./appService";
import { Observable, observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {
  private dbPath = '/services';
  serviceRef: AngularFireList<AppService> = null;

  constructor(private db: AngularFireDatabase) {
    this.serviceRef = db.list(this.dbPath);
  }

  createCategory(service: AppService): void {
    this.serviceRef.push(service);
  }

  updateCustomer(key: string, value: any): Promise<void> {
    return this.serviceRef.update(key, value);
  }

  deleteCustomer(key: string): Promise<void> {
    return this.serviceRef.remove(key);
  }

  getCategoryList(): AngularFireList<AppService> {
    return this.serviceRef;
  }

  deleteAll(): Promise<void> {
    return this.serviceRef.remove();
  }

}
