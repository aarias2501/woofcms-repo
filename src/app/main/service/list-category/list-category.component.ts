import { Component, OnInit, Injectable } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';


import { Category } from "../Category";
import { CategoryService } from '../category.service';

import { locale as english } from '../i18n/en';
import { locale as turkish } from '../i18n/tr';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

import { map, filter, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DeleteComponent } from '../delete/delete.component';
import { ifError } from 'assert';


@Component({
  selector: 'list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})

export class ListCategoryComponent
{
  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  
  //  ref: AngularFirestoreCollection<Category>;
   
   items: Observable<any[]>;
   ids: any[];
   
   list: Category[];
   todoDetails : AngularFireObject<Category>;

  itemRef: AngularFireObject<any>;
  itemAux: Observable<any>;
  
  todo$: Observable<Category[]>;
  clean: any[]; 


  constructor(

    public db: AngularFireDatabase,
    private categoryService: CategoryService,
    private router: Router,
    public dialog: MatDialog,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService
  )
  {  
    this.itemRef = db.object('superCategory');
    this.itemRef.snapshotChanges().subscribe(action => {    
      var aux: any[] = action.payload.val();
      this.clean = [];
      for(var index in aux) {        
        this.clean.push({
          id: index,
          name: aux[index].name,
          description: aux[index].description,
          picture: aux[index].picture,
        });
      }
      });
    this._fuseTranslationLoaderService.loadTranslations(english, turkish);
 }

 edit(id: string){
  this.router.navigate(['service/editCategory/' + id]);
 }

 delete(id: string, collection: string){
  let delRef: AngularFireObject<any> = this.db.object(collection + '/' + id);
  delRef.remove();
  if(collection == 'superCategory'){
    this.router.navigate(['service/listCategory']);
  }
  if(collection == 'service'){
    this.router.navigate(['service/listService']);
  }
 }

 deleteConfirm (inId: string, inCollection: string): void {
  const dialogRef = this.dialog.open(DeleteComponent, {      
    data: {id: inId, collection: inCollection}
  });

  dialogRef.afterClosed().subscribe(result => {
    if(result.id){
      this.delete(result.id, result.collection);
    }
  });
}
}