import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceComponent } from './service.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { AddServiceComponent } from './add-service/add-service.component';
import { ListServiceComponent } from './list-service/list-service.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { ListCategoryComponent } from './list-category/list-category.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';



/* FORM HANDLING */
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { DeleteComponent } from './delete/delete.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { EditServiceComponent } from './edit-service/edit-service.component';

const routes = [
  {
      path     : 'service',
      component: ServiceComponent,
  },
  {
      path     : 'service/addService',
      component: AddServiceComponent,
  },
  {
      path     : 'service/listService',
      component: ListServiceComponent,
  },
  {
      path     : 'service/addCategory',
      component: AddCategoryComponent,
  },
  {
      path     : 'service/listCategory',
      component: ListCategoryComponent,
  },
  {
      path     : 'service/editCategory/:id',
      component: EditCategoryComponent,
  },
  {
      path     : 'service/editService/:id',
    component: EditServiceComponent,
}

];

@NgModule({
  declarations: [
    ServiceComponent,
    AddServiceComponent,
    ListServiceComponent,
    AddCategoryComponent,
    ListCategoryComponent,
    DeleteComponent,
    EditCategoryComponent,
    EditServiceComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    FuseSharedModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule
  ],
  exports: [
    ServiceComponent,
    AddServiceComponent,
    ListServiceComponent,
    AddCategoryComponent,
    ListCategoryComponent
  ],
  entryComponents :[
    DeleteComponent,
  ]
})
export class ServiceModule { }
