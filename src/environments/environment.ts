// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production  : false,
    hmr         : false,
    firebase    : {
        apiKey: "AIzaSyBLhE8e_BaxcHph6BgUnM6xX4FRPFCpLMA",
        authDomain: "woof-57bd5.firebaseapp.com",
        databaseURL: "https://woof-57bd5.firebaseio.com",
        projectId: "woof-57bd5",
        storageBucket: "woof-57bd5.appspot.com",
        messagingSenderId: "75464101401",
        appId: "1:75464101401:web:87cdef7c6bf32e685d6488",
        measurementId: "G-YT3B8TYE3G"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
